---
title: The Annual Royal Artisan Competition
subtitle: Updated December 7th, A.S. XLVIII
---
<p>The Royal Artisan Competition is held to select the Crown’s Royal Artisan. &nbsp;It shall be held in multiple stages throughout the year at the following events: &nbsp;Twelfth Night/Midwinter Coronation, &nbsp;Spring Crown Tournament, Midsummer Coronation, and Fall Crown Tourney. These events will be referred to as Competition Events throughout this document. The cycle will begin at Twelfth Night and conclude at Fall Crown, where the winners will be announced. &nbsp;The Royal Artisan will then be invested in a formal ceremony at Kingdom University. The competitions are drawn from eight general A&amp;S categories (culinary arts, fine arts, textile arts, performance arts, martial sciences, decorative sciences, functional sciences and research). Additionally, prizes will be awarded in two special categories (under the age of 12 and between the ages of 12 and 16). Each general category is broken into the subcategories.</p>
<h3>Categories</h3>
<p><strong>Culinary Arts</strong></p>
<ul>
<li> Brewing/Vinting </li>
<li> Cooking, Single Dish </li>
<li> Cooking, Subtlety </li>
<li> Cooking, Course </li>
</ul>
<p><strong>Fine Arts</strong></p>
<ul>
<li> Calligraphy </li>
<li> Games, Toys and Dolls </li>
<li> Heraldic Display </li>
<li> Illumination </li>
<li> Painting </li>
<li> Sculpture </li>
<li> Stained Glass </li>
</ul>
<p><strong>Textile Arts</strong></p>
<ul>
<li> Accessory, Clothing </li>
<li> Clothing, Early Period (600 AD - 1350 AD) </li>
<li> Clothing, Middle Period (1351 - 1450 AD) </li>
<li> Clothing, Late Period (1451 AD - 1600 AD) </li>
<li> Dyeing </li>
<li> Embroidery, Counted </li>
<li> Embroidery, Freeform </li>
<li> Felt Making </li>
<li> Knitting, Crochet, Macrame </li>
<li> Lacemaking </li>
<li> Needlework, Miscellaneous </li>
<li> Spinning </li>
<li> Weaving, Band/Inkle/Tablet </li>
<li> Weaving, Standard </li>
</ul>
<p><strong>Performance</strong></p>
<ul>
<li> Bardic Performance </li>
<li> Dancing, Eastern </li>
<li> Dancing, European </li>
<li> Dramatic Performance </li>
<li> Musical Performance, Instrumental </li>
<li> Musical Performance, Vocal </li>
<li> Poetry </li>
</ul>
<p><strong>Martial Sciences</strong></p>
<ul>
<li> Archery Equipment </li>
<li> Armor </li>
<li>Chainmail</li>
<li>Metalwork</li>
<li>Weaponry</li>
</ul>
<p><strong style="line-height: 22.0499992370605px;">Decorative Sciences</strong></p>
<ul>
<li> Book Binding </li>
<li>Enameling</li>
<li>Glasswork</li>
<li>Jewelry</li>
</ul>
<p><strong style="line-height: 22.0499992370605px;">Functional Sciences</strong></p>
<ul>
<li> Ceramics and Pottery</li>
<li> Chandlery</li>
<li> Leatherwork </li>
<li> Material Preparation</li>
<li> Paper Making </li>
<li> Tents</li>
<li> Woodwork </li>
</ul>
<p><strong style="line-height: 22.0499992370605px;">Research Papers</strong>**</p>
<p>** This is the category for research on any period topic. Research papers do not include items, only research. The important part of this category is your research; what you did, how you did it, the sources you used, and your conclusions. There is not a set format for a research paper, but bear in mind that the judges will have to read your work in order to judge it.</p>
<h3>Entering the Competition</h3>
<p><strong><em>All research papers must be submitted in English.</em></strong> Entrants must send the paper in a DOC, TXT, RTF or PDF format to MOAS Email (<script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,109,111,97,115,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,39,62,109,111,97,115,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,60,47,97,62));</script>) <strong><em>no later than the Friday three weeks before the event, so it can be sent to the judges</em></strong>.  Entries from any category will be accepted at each Competition Event. Individuals can enter any competition they wish, and can enter as many items as they like. However, only the highest scoring item from a given category will be used for determination of the A&amp;S Championship. In other words, if an individual enters three different meads as separate items, the highest score of the three will be used as the score for the Culinary Arts category for that individual.  <a href="{{ site.baseurl }}{% link offices/moas/royal-artisan-qualifications.md %}">Click here for a summary of the requirements and entry information.</a></p>
<h3>Competition Guidelines</h3>
<p>If you are going to enter an item into the Arts and Sciences competition you need to enter it with documentation and a competition form. The competition form is at the Arts and Sciences Judging Criteria web page for the kingdom. You must have completed the item you enter into the competition while residing in the Kingdom and it cannot have been entered in any previous Kingdom A&amp;S Competition or Competition Event.  If you can't attend the event, you can send someone that will represent you. If possible, please inform the MoAS in advance who will be representing you., so your item(s) can be returned to the proper person.  After the event, the MoAS will either e-mail or postal mail you your score sheets.  You must send a letter of intent to the Kingdom MoAS if you plan on competing in the A&amp;S Championship Tourney at least <strong>two weeks in advance of the Competition</strong> event via e-mail. The notification must include your SCA and mundane names, the number of items you plan to enter, a brief description of each item, and the category into which the item will be entered. This advance notification will assist in selecting judges expert in the areas represented and in scheduling the judging during the event.  You can also enter as many items as you like, but each item needs to be entered with its own documentation. If you like, you can have more than one item for your entry but in that case it will be counted as a single entry with your documentation. This might be a way to show a special technique (like the different stages of making a gauntlet) or to better illustrate a complex manufacturing process. You cannot enter an item more than once by itself. But you can incorporate an already entered item as part of a larger project at a later occasion. If you include it as part of a larger project, the item must be specifically identified in your documentation.  All items must be entered by Saturday morning. However, keep in mind we intend to start judging Friday night., so anything that can be entered Friday evening should be. This will give the judges more time to look at your entry.  Only items that score above 50 will be counted towards the grand total.</p>
<h3>Display and Display/Feedback Items</h3>
<p>If you do not wish to enter the competition, but want people to look at your work, you can enter it for display only. That means that it will be put out for people to look at, but it will not be judged nor will you receive any feedback on it.  Your item can also be entered as Display/Feedback. In that case it will not be formally judged, but the judges will give you written feedback on how you can improve your work.  Any A&amp;S item can be entered in Display or Display/Feedback and it does not have to have been finished in kingdom, and it could have been entered in a previous kingdom A&amp;S competition.</p>
<h3>Special Categories</h3>
<p>The special categories have some special requirements and rules. You need to fulfill the requirements if you want to enter into one of the special categories. For requirements and rules please see the "Special Categories" information below. At the end of the year, one prize will be awarded for the best item from each of these special categories. These categories are separate from the Kingdom A&amp;S Championship, and any item entered in them will not be considered for that competition.</p>
<h3>Under 12 Category</h3>
<p>May enter with items from all subcategories from above. You need to be younger than 12 years old at the day of the competition to enter this category. The item should be entered with a small amount of documentation that includes: SCA name, modern name, name of item, country of origin, period of origin and if the item is made with or without help. The important part is the item and not the documentation.</p>
<h3>12 to 16 Category</h3>
<p>May enter with items from all subcategories from above. You need to be between the ages of 12 to 16 at the day of the competition to enter this category. The item should be entered with a documentation that includes at a minimum: SCA name, modern name, name of item, description of item, country of origin, period of origin, and sources. You should also acknowledge anyone who assisted you with making the item.</p>
<h3>Documentation</h3>
<p>Documentation must be written in English and be presented at the same time as the item. Your documentation at minimum must include: SCA name, modern name, SCA group, membership number, name of item, general and sub-category to which item belongs, description of item, description of materials used, description of manufacturing process, country of origin, period of origin, and sources.  While there is no maximum for documentation, keep in mind the judges have to read your work at the event. Be sure to include both your SCA and mundane names on your documentation.  Documentation should be written in a standard font size. Standard font size means a font between 10 and 12. Please don't use calligraphic or other hard to read fonts. You may use larger or smaller font in special cases like footnotes, headers and so on. The entry may be hand written, but it needs to be legible. Documentation which does not follow this format may receive fewer points than it otherwise would have.  <strong>Remember: Documentation is an important part of your score!</strong> You must submit <strong>four copies</strong> of your documentation with the entry so the judges can each read one at the same time. If you have more than four copies, you could always give a copy away to those interested in your item or project.  More information on how to write documentation is available on the Drachenwald website.</p>
<h3>Performance Art Entries</h3>
<p>Individuals entering performance pieces in a competition will be notified at the event as to when and where their entry will be judged. Additionally a list of performance times and locations will be posted at the Troll. Bear in mind that all performances will be open to the populace.</p>
<h3>About the Judges and Judging Sheets</h3>
<p>We will make every effort to find judges who are knowledgeable about the particular topic of the competition, but we are limited to those individuals present at the event and available at the time of the competition.  Judges are required to sign their names on the judging sheets, and are asked to provide written commentary about the entry. All judging sheets are returned to the representative of the MoAS in charge of the Competition event. We consider judging forms to be private, so they will be returned directly to the entrant, and are not available for perusal by other entrants or by the populace. If they are not picked up by the entrant, they can be retrieved from the appropriate A&amp;S Officer at a subsequent event, or can be requested via e-mail.  Upon the judges discretion, and depending upon availability and time, judges can be made available for one-on-one discussion of entries and judging results. Please contact the A&amp;S Officer in charge of the event if interested in such feedback.</p>
<h3>Prizes</h3>
<p>At each Competition Event a prize will be awarded for the highest-scoring item overall. The entrants will be recognized at court and presented with a token acknowledging their achievement. This award is for that event only. The overall winner of each Competition Event will receive an additional 5 points towards their score for the Kingdom A&amp;S Championship. At Kingdom University, a prize will be awarded for each regular category and each special category. The winner for each category will be the individual with the most points in that category throughout the year. Again the winners will be presented with a token of acknowledgement.</p>
<h3>The Annual Arts and Sciences Championship</h3>
<p>All the Competition Events lead to the Annual Arts and Sciences Championship. The competition begins at Twelfth Night and concludes at Kingdom University.&nbsp;It is recommended you declare your intent to compete for the Kingdom A&amp;S Championship by sending a letter/e-mail to the Kingdom MoAS at least two weeks prior to your first Competition Event.&nbsp;To qualify for consideration you must:</p>
<ul>
<li> Enter one item in at least three of the six general categories scoring at least 50 points. </li>
<li> Have an entry in a minimum of two Competition Events. </li>
<li> The winner will be the individual who receives the highest combined point total. </li>
</ul>
<p>You can enter as many items as you like, but only your highest entry from any one category will be considered for the Championship.  <a href="{{ site.baseurl }}{% link offices/moas/royal-artisan-qualifications.md %}">Click here for a summary of the requirements and entry information.</a> During the final court at Kingdom University, the Championship winner will be announced and designated "Royal Artisan." As Royal Artisan, the winner will assist the Queen and Minister of Arts and Sciences in promoting the Arts and Sciences.</p>
