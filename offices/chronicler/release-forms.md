---
title: Publication Release Forms
---
<h3>Publication Release Forms</h3>
<p>SCA Policy requires that release forms be completed for material that is to be published in an official SCA newsletter (or other publication) and on SCA websites.</p>
<p>The Creative Release Form is to be used for any piece of orginal work - an article, story, poem, piece of artwork etc.&nbsp; It must be completed and signed by the creator(s) of the work.&nbsp; This is not needed for officer letters, announcements about policy etc or event announcements, unless the latter are in the form of an original work, e.g. a poem, or contain original artwork.</p>
<p>The Photographer Release Form is to be used for any original photograph (whether or not it includes people) that is to be used in a publication or on a website.&nbsp; It must be completed by the photographer.</p>
<p>The Model Release Form is to be used where any person in the photograph is recognisable by their facial features, and must be completed by all the people present in the photo.&nbsp; It must be used for any photograph appearing in a print publication.&nbsp; It is not needed for photographs on websites if they were taken at an event where there is no expectation of privacy - i.e. a site to which the public could have access. A release would be needed if the photograph was taken at a private part of the event (e.g. a peerage vigil), or there was no expectation of public access (e.g. an event in a private home).&nbsp; Note that the model release form has an option for a blanket release (i.e. you can use any picture of me any time).&nbsp; Encourage people to use this option.</p>
<p>There are two versions of each form.&nbsp; A traditional one, which must be completed and signed by the person giving the release and then passed to the publisher of the item (chronicler, web minister etc), and one that can be completed electronically and signed electronically.&nbsp; The latter need Adobe Reader to be installed. The completed forms can then be emailed to the published of the item concerned.</p>
<p>See the Q&amp;A document below for further details.</p>
<p>These forms MUST be used for all SCA newsletters and websites, from January 2011 onwards.</p>
<p>To download and save the forms for local use, right click on the link and select Save Link as (or equivalent on other browsers).</p>
<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/ReleaseCreative.pdf %}">Creative Release Form</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/ReleaseCreativeFillable.pdf %}">Creative Release Form Electronic</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/ReleasePhotographer.pdf %}">Photographer Release Form</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/ReleasePhotographerFillable.pdf %}">Photographer Release Form Electronic</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/ReleaseModel.pdf %}">Model Release Form</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/ReleaseModelFillable.pdf %}">Model Release Form Electronic</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/qa.pdf %}">Questions &amp; Answers</a></li>
</ul>
<p>If you have any questions, please contact the Chronicler or Web Minister as appropriate.</p>
