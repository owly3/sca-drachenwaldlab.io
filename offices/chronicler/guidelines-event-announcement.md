---
title: Event announcement guidelines
---
<table>
<tbody>
<tr>
<td style="font-size: 10px; text-align: center;"><a href="#first">Schedule the Event</a></td>
<td style="font-size: 10px; text-align: center;"><a href="#write">Write the Announcement</a></td>
<td style="font-size: 10px; text-align: center;"><a href="#req">Required Elements</a></td>
<td style="font-size: 10px; text-align: center;"><a href="#opt">Optional Elements</a></td>
<td style="font-size: 10px; text-align: center;"><a href="#event">Example Announcement</a></td>
<td style="font-size: 10px; text-align: center;"><a href="#mini">Example Mini-Announcement</a></td>
</tr>
</tbody>
</table>
<p>Event announcements are the formal method of notifying the populace of events that are scheduled on the Kingdom Calendar.&nbsp; It also make the event "<a href="/content/making-your-event-official">official</a>" for the purpose of Kingdom business .&nbsp; If the event is not on the Kingdom Calendar then an event announcement is not needed (except maybe in your local newsletter).</p>
<h3>First, Schedule Your Event</h3>
<p>Your event must be scheduled on the Kingdom Calendar <span style="text-decoration: underline;">before</span> submitting your event announcement.&nbsp; Events are scheduled using the <a href="http://www.drachenwald.sca.org/wordpress/kingdom-calendar/calendar-request/">Event Date Request Form</a>.&nbsp; You should wait until you receive confirmation that the date is accepted before sending the announcement.</p>
<p>Your announcement must appear in the issue of <em>Dragon's Tale</em> for the month the event occurs. If the event is in the first 10 days or so, then it should appear the previous month as well.&nbsp; </p>
<p>Dragon's Tale policy is that event announcements for this month and next month will always appear in the current issue.&nbsp; Announcements for events further in the future may appear, subject to space available, with priority being given to Kingdom events.&nbsp; To ensure advance notice of an event, a mini-announcement may be submitted.&nbsp; See <a href="#mini">below</a> for an example. <a name="write"></a></p>
<h3>Writing Your Announcement</h3>
<p>Event announcements should be kept simple and to the point.&nbsp; They should contain sufficient information to help people decide if they want to attend or not.&nbsp; The information listed <a href="#must">below</a> <span style="text-decoration: underline;">must</span> be included.&nbsp; Detailed driving directions, timetables of events etc are best left to an event website, or to be emailed to those reserving for the event. Announcements will be edited to standardise content and length.&nbsp; An example announcement is shown <a href="#event">below</a>.</p>
<p>Announcements should be sent in plain, unformatted Word or RTF format, preferably sent as an email attachment.&nbsp; Be wary of accented characters, which do not always transfer well in email or plain text.&nbsp; Do not send elaborately laid out flyers, as these make it much harder to edit the copy into the newsletter, and anyway, the formatting won't be used anyway.</p>
<p>Before submitting your announcement, please check to ensure you have included all the required elements listed <a href="#must">below</a>.</p>
<p>Send the announcements to the Chronicler using the <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,99,104,114,111,110,105,99,108,101,114,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,39,62,99,104,114,111,110,105,99,108,101,114,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,60,47,97,62));</script> address.&nbsp; Mark your announcement with "[DT] Name of Event" in the subject.</p>
<p>The deadline for event announcements is the 25th of the month, two months before the issue date, e.g. the deadline for the December issue is 25th October.<br /><a name="req"></a></p>
<h3>Required Elements</h3>
<p><strong>Event announcements <span style="text-decoration: underline;">must</span> include:</strong></p>
<ul>
<li> Name of official SCA sponsoring group.  (Where an event is hosted by an incipient group, their name must be listed, as well as that of the official sponsoring group)</li>
<li> Legal name (both Society and modern), phone and email to the autocrat/Event Steward. </li>
<li> Street address of the site (if no address, you must mention that in the ad , "No site address" works fine). </li>
<li> Date of the event. </li>
<li> Starting/stopping time of the event . </li>
<li> Payment information (if there is a fee).</li>
</ul>
<p><a name="opt"></a></p>
<h3>Optional Elements</h3>
<p><strong>You are encouraged to include:</strong></p>
<ul>
<li>Name and contact details of Marshal-in-Charge (if there is combat). </li>
<li>A description of the event (teaching event, tournament, A&amp;S event etc)</li>
<li>What meals are provided</li>
<li>Information about the site facilities (camping, crash space, bunks, bedding supplied etc)</li>
<li>Travel information (Sufficient to help visitors to assess the amount of travel involved, but not detailed turn-by-turn directions)</li>
<li> Reservation deadlines, if any </li>
<li> URL for the the event website, if there is one (a good place for detailed directions etc) </li>
<li> Restrictions, if any (pets, open fires, alcohol)</li>
<li>Contact information for other event staff (cook, A&amp;S coordinator, class coordinator etc)</li>
</ul>
<p><a name="event"></a></p>
<h3>Example Announcement</h3>
<p><em><strong>Feast of Fools</strong><br />10-12 January 2050 - Seven Trees - England</em></p>
<p>Come one and all and join the shire of Seven Trees in our weekend of feasting and frolics. Purchase a fighter in the Foolish Tournament in the afternoon, or test your merry quips against all-comers for the honour of being the Fool at the Feast.&nbsp; There will also be a competition for the silliest period(ish) hat.</p>
<p><strong>Site</strong><br />The site is the Lidden Tree Scout Camp, Green Lane, Riverford, Kent&nbsp; KE99 K99.&nbsp; The site has 64 bunks, spread over two "log cabin" buildings, plus crash space on the floor.&nbsp; There is ample space for camping.&nbsp; Showers and toilets are available,&nbsp; A bottom sheet only is provided, so bring sleeping bags and pillows.<br />Site opens at 18:00 on Friday 10th and closes 14:00 on Sunday 12th<br />The site is discreetly wet.&nbsp; No pets except guide dogs.</p>
<p>The site is 20 miles south of Canterbury.&nbsp; Nearest main airport is Gatwick.&nbsp; Nearest ferry port is Dover.</p>
<p><strong>Costs</strong></p>
<table style="width: 262px; height: 155px;" border="0" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td width="200"><em>Weekend, with bunk<br /></em></td>
<td width="50"><em>£25</em></td>
</tr>
<tr>
<td width="200"><em>Weekend, crash space<br /></em></td>
<td width="50"><em>£23</em></td>
</tr>
<tr>
<td width="200"><em>Weekend, camping<br /></em></td>
<td width="50"><em>£23</em></td>
</tr>
<tr>
<td width="200"><em>Day trip with feast<br /></em></td>
<td width="50"><em>£18</em></td>
</tr>
<tr>
<td width="200"><em>Day trip (no feast)<br /></em></td>
<td width="50"><em>£12</em></td>
</tr>
<tr>
<td><em>Family Cap<br /></em></td>
<td width="50"><em>£60</em></td>
</tr>
<tr>
<td width="200"><em>Kids under 5<br /></em></td>
<td width="50"><em>Free</em></td>
</tr>
</tbody>
</table>
<p><em>Make cheques payable to SCA Seven Trees.&nbsp; Send reservations to the reservations steward, stating modern &amp; SCA names, SCA membership no, any food allergies.</em></p>
<p><strong>Event Steward</strong><br />Lord Barry the Bonehead (Neville Smithers)<br />14, Acacia Avenue, Smellytown, Kent KK88 K88<br />+44 (0)1234 567890<br /><script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,114,121,64,98,111,110,101,104,101,97,100,46,111,114,103,39,62,98,97,114,114,121,64,98,111,110,101,104,101,97,100,46,111,114,103,60,47,97,62));</script></p>
<p><strong>Reservations</strong><br />Blodwyn Pigge (Alice Band)<br />address as above<br /><script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,97,108,105,99,101,64,98,111,110,101,104,101,97,100,46,111,114,103,39,62,97,108,105,99,101,64,98,111,110,101,104,101,97,100,46,111,114,103,60,47,97,62));</script></p>
<p><strong>Marshal in Charge</strong><br />Sir Cumference (Jerry Bilt)<br /><script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,106,101,114,114,121,64,99,111,119,98,111,121,99,111,110,116,114,97,99,116,111,114,115,46,99,111,109,39,62,106,101,114,114,121,64,99,111,119,98,111,121,99,111,110,116,114,97,99,116,111,114,115,46,99,111,109,60,47,97,62));</script><br /><br /> <a name="mini"></a></p>
<h3>Example Mini Announcement</h3>
<p><em><strong>Feast of Fools</strong> - 10-12 Jan 2050 - Seven Trees - England<br />Join us for tournaments, jesting, and a feast with humorous entertainment.&nbsp; Plus, silly hats.<br />Contact Barry the Bonehead (Neville Smithers) <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,98,97,114,114,121,64,98,111,110,101,104,101,97,100,46,111,114,103,39,62,98,97,114,114,121,64,98,111,110,101,104,101,97,100,46,111,114,103,60,47,97,62));</script></em></p>
