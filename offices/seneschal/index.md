---
title: Office of the Seneschal
---

Welcome! In this page you should find everything you need to help in the smooth administration of groups within Drachenwald. Here you will find our Great Book of Laws, warrants, policies and the forms which must be completed for Shire Quarterly reports.


<a name="Corpora"></a>
## Corporate Documents

* [SCA Inc. Seneschal Resources](http://socsen.sca.org/kingdoms-and-seneschals/seneschal-resources/) including the SCA Inc. Organisational Handbook and the Society Senechals Handbook.

{% comment %}
* <a href="http://www.sca.org/docs/pdf/govdocs.pdf" title="Offsite Link - SCA.ORG">SCA, Inc. Organisational Handbook</a>&nbsp;(PDF - Jan 2014)cd
* <a href="http://www.sca.org/docs/pdf/KingdomSeneschalsHandbook.pdf" title="Offsite Link - SCA.ORG">Society Seneschals Handbook</a> (PDF - Jul 2013)
{% endcomment %}

<a name="DW"></a>
## Kingdom Documents

* [Warrant of Appointment to Office]({{ site.baseurl }}{% link offices/seneschal/files/warrant_blank_feb_2018.doc %}) (DOC)
* [Kingdom Law]({{ site.baseurl }}{% link offices/seneschal/files/drachenwald-law-jan2019.pdf %}) (PDF)
* [Seneschal Policies]({{ site.baseurl }}{% link offices/seneschal/seneschal-policies.md %})
* [Bidding for a crown event in Drachenwald]({{ site.baseurl }}{% link offices/seneschal/event-bid-checklist.md %})
* [Seneschals Handbook]({{ site.baseurl }}{% link offices/seneschal/seneschals-handbook.md %})
* [Youth Policy Appendix]({{ site.baseurl }}{% link offices/seneschal/files/youthpolicyappendix.pdf %}) (PDF)
* [Participate in Crown Tourney]({{ site.baseurl }}{% link offices/seneschal/participate-in-crown-tourney.md %})



{% include officer-contacts.html %}
