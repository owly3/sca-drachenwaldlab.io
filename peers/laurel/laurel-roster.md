---
title: Order of the Laurel
toc: true
toc_label: Members
excerpt: Roster of members
---

## Alphabetical Roster

## Mistress Anna Laresdotter

Region: Nordmark, Shire of Holmrike

Primary artforms/Elevated for: History of knitting and 16th century clothing.

Dabbles in: Childrens clothing, Braiding, dancing and fencing.

Contact: At events or anna.gunnar at gmail.com

Apprentices/Students/Household affiliations: N/A

Elevated by whom and when: Sven and Siobhan, Styringheim Baronial Investiture 2013-12-14

Classes I have held/are willing to hold: A lot :). Knitting, sewing, simple braiding. Feel free to ask me.

## Master Alexandre Lerot d'Avigné

Region: Insulae Draconis, Shire of Flintheath

Primary artforms/Elevated for: Cookery with a side-order of poetry. Also meat curing and cheesemaking.

Dabbles in: a great many things.

Contact: At events or nexus@panix.com

Online presence: www.aspiringluddite.com

Apprentices/Students/Household affiliations: Households - St. Mary's Abbey (Outlands), Sharc Pit (rather more widely dispersed). Pandiscipuli - Alessandra Bruscioli, called Bruce; Mael Eoin mac Echuid; Aodhan dha Cheist; Milada von Schnecken

Elevated by whom and when: Andreas IV and Gabriella II, 08/06/2009, Pennsic War

Classes I have held/are willing to hold:cookery, sausage making, court heraldry, documentation.

## Master Arenwald von Hagenburg

Region: Nordmark, Shire of Aros

Primary artforms/Elevated for: Mainly woodwork, but also for different craft projects, fencing, dancing and singing: for being a renaissance man.

Dabbles in: making armour, leatherwork, woodwork, singing, dancing, making costumes, making tents, casting  etc

Contact: At events

Apprentices/Students/Household affiliations: Apprentice - Náttfari Gardarsson

Elevated by whom and when: Jade and Shaheena, Kingdom of the West, 11th of April AS 27, Rowany Festival in Lochac 1993.

Classes I have held/are willing to hold: Anything to do with craft and music!

## Bonesig Arianhwy Wen/Magistra Mala
Region: Insulae Draconis, West Dragonshire

Primary artforms/Elevated for:  scribal arts; poetry, prose, and drama (as scholar and author); languages; heraldry, particularly voice heraldry; anything to do with words; acting and performance

Dabbles in: archery, gaming, brewing

Contact: daylightjam at gmail dot com 

Apprentices/Students/Household affiliations: Brighthelm, seynt Scholastes Hall

Elevated by whom and when: Vitus III and Isabel II, Raglan, Aug 8, 2018

## Dame Aryanhwy merch Catmael

Region: Insulae Draconis, Shire of Depedene-under-Wychwood

Primary artforms/Elevated for: Research (Names)

Dabbles in: Charcuterie, calligraphy, illumination, heraldry

Contact: At events

Online presence: http://dmnes.org and http://scriptura-et-pictura.blogspot.com/

Elevated by whom and when: Vitus III and Isabel II, Raglan Fair, August 2018

Classes I have held/are willing to hold: Classes on medieval naming and heraldic practices, classes on SCA naming and heraldic practices, classes on scribal arts, including scroll wordings.

## Mestari Beata Sigridsdotter
Region: Barony of Aarnimetsä, Canton of Hukka

Primary artforms/Elevated for: Early period Costume/Embroidery

Dabbles in: Viking posaments and metal wire decorations

Contact: At events or facebook

Apprentices/Students/Household affiliations: N/A

Apprenticed to: Duchess Caoimhe ingen Domnaille from An Tir

Elevated by whom and when: Lief and Morrigan, Aarnimetsä Midwinter, 2018

Classes I have held/are willing to hold: Viking posament and metal wire decorations (ösenstitch, schlingen stitch), Viking embroidery, Finnish iron age bronze spiral decorations, Tasselbelt from Luistari, Tablet weaving for beginners, Metal wire brokade tablet weaving. Feel free to ask.

## Mistress Bridget Greywolf
Region: Knight’s Crossing, Canton Aventiure 

Primary artforms/Elevated for: Calligraphy and Illumination

Dabbles in: Stuff

Contact: At events or merlyn.gabriel@googlemail.com

Online presence: Regularly posts to http://dragonscribes.blogspot.de/

Apprentices/Students/Household affiliations: Apprentices: Lady Saraswati mán ikkam

Elevated by whom and when: 2007-03 by Maximilian and Margerite at Saturday Night Fever III

Apprenticed to: Master Harold von Auerbach (East Kingdom)

## Master Cernac the Navigator

Region: Insulae Draconis, Shire of Dun in Mara

Primary artforms/Elevated for: Historical Combat

Contact: At events

Elevated by whom and when: Gerhardt and Judith in AS XLVI (2010)

Classes I have held/are willing to hold: Period longsword techniques

## Mistress Ellisa von Berenklaw (formerly Ailitha de Ainwyck)

Region: Frankmark

Primary artforms/Elevated for: Calligraphy and Illumination (all gothic hands (from textura quadrata to bastarde and rotunda), illuminations from the 12th to 16th century from mainland Europe (Germany, Tschech republic, Austria, France and Italy), making paint from pigments)

Dabbles in: a great many things.

Contact: At events or angelanelk@Hotmail.de, on Facebook you look for Angela Nelk

Apprentices/ Students/ Household affiliations: Apprentice - Lady Swanhilde von Baerenau

Elevated by whom and when: Thorvaldr and Tomoe at Crown Tourney, A.S. L (50) in the Shire of Polderslot.

Apprenticed to: Bridget Greywolf

Classes I have held/are willing to hold: Gilding with gold leaf and sugar gesso, How to start a scroll (preparing the paper, layout and lines, tips and tricks), shading techniques.

## Mistress Eva Grelsdotter
Region: Aarnimetsä, Canton of Poukka

Primary artforms/Elevated for: Cooking

Dabbles in: Viking clothing, Finnish Iron Age clothing, 15th century clothing, 16th century clothing (especially Flemish), blackwork, tablet weaving and naalbinding

Contact: At events or at saara.nironen@suomi24.fi

Online presence: http://lethemboyle.com http://evagrelsdotter.blogspot.fi

Apprentices/Students/Household affiliations: Apprentice - Lady Magdalen Yrjänäntytär

Elevated by whom and when: 7 of September 2013 by Sven and Siobhan

Apprenticed to: Katheryn Hebenstreitz

Classes I have held/are willing to hold: How to modify medieval recipes to modern, feast planning (always willing to help with finding recipes or help with planning one on one if needed), blackwork and naalbinding for beginners

## Master Fardäng Skvaldre

Region: Nordmark, Shire of Gyllengran

Primary artforms/Elevated for: Brewing, vintning and infusions

Dabbles in: Making armour, tents and costumes (primarily viking and 16th century), Cooking, leather work and more.

Contact: At events or fardang@gmail.com

Apprentices/Students/Household affiliations: Apprentice: Sigvard Ölfúss. Fardäng is also a squire to chevalier Armand.

Elevated by whom and when: Vitus and Isabel at Double Wars, A.S. 51

Classes I have held/are willing to hold: Brewing in both theory and practice. History and tradition regarding beverages.

## Master Giano Balestriere

Region: Frankmark

Primary artforms/Elevated for: Culinary arts

Dabbles in: Cosmetics, paints, adhesives.

Contact: At events

Elevated by whom and when: Gerhardt and Judith, Störtebeker's Sons 2009

Apprenticed to: Jaelle of Armida

Classes I have held/are willing to hold: Period Cooking for Beginners, Period Camp Cookery, Roman Foodways, Dark Age Foodways, Carolingian Foodways, Landsknecht Cookery, Period Cosmetics, Period Paints, Period Medicine for Beginners, How to Make a Roman Persona, Language and Literacy in Period

## Meister Guntram von Wolkenstein

Region: Southern Reaches, Adamastor

Primary artforms/Elevated for: Tabletweaving

Dabbles in:  Pewter & bronze casting, calligraphy & illumination, leatherwork, heraldry, lampworked beads, very occasional brewing

Contact: At events or guntram@guntram.co.za

Online Presence: http://guntram.co.za/tabletweaving

Elevated by whom and when:  John and Honor at Coronation - Last Court, Aarnimetsä, 08 Jan 2005

Apprenticed to: Mistress Mirabel Belchere, East Kingdom.

Classes I have held/are willing to hold: Tabletweaving, pewter casting, beadmaking, calligraphy & illumination, leatherwork

## Master Gwylym Penbras

Region: Frankmark

Primary artforms/Elevated for: Knife and sword forging no real timeframe. I make blades per the person’s persona.

Contact: At events

Elevated by whom and when: Middle Kingdom 2006

## Mistress Helena von Eltz

Region: Aarnimetsä

Primary artforms/Elevated for: 14th century tailoring

Dabbles in: Headwear (especially frilled veils) dress accessories, embroidery, research, handsewing. Anything 14th century.

Contact: At events + see website for contact details

Online presence: http://www.neulakko.net/

Apprentices/Students/Household affiliations: Lord Helgi, apprentice 

Elevated by whom and when: 2010 by UlfR and Caoimhe

Classes I have held/are willing to hold: Clothes from Archaeological excavations 
From Wimple to Poulaine: the outfit of a 14th century lady
The Medieval outfit: an overview of medieval dress – what was worn and what did it mean?
High Fashion from the High Middle Ages
Female headdress: veils, caps, hoods
Hoods: history, styles and patterns
How to wear a veil
Frilled veils: history and how to make one for yourself
Different sources: archaeology, written evidence and art
Drafting a pattern for a Herjolfsnes-gown
Drafting your own pattern for hose (workshop)
Handsewing and tailoring techniques (lecture and workshop)
Drafting a sleeve pattern for a buttoned-up sleeve, making cloth buttons (workshop)
Making a self-supportive underdress (workshop)

## Mistress Helwig Ulfsdotter
Region: Nordmark, Shire of Aros

Primary artforms/Elevated for: Northern european 16th century costuming

Dabbles in: knitting, embroidery, early clothing, 14th century clothing

Contact: At events or mistresshelwig@gmail.com

Apprentices: Filippa Birgersdotter, William of Richwood, Isabetta del Verde. Aleydis van Vilvoorden. Head of House Three Scissors and Duck

Elevated by whom and when: Marcus and Cecilia in 2009

Classes I have held/are willing to hold: Tailoring from 16th century sources
How to knit a 16th century bonnet
Beginning black work
More advanced black work
The renaissance household, food and beverages

## Mistress Joutsenjärven Sahra

Region: Barony of Aarnimetsä

Primary artforms/Elevated for: Tablet weaving and Finnish Iron Age

Dabbles in: I do weaving, with tablets and looms, natural dyeing, nalbinding, sewing and writing books, I have published a tablet weaving book called Applesies and Fox Noses (Salakirjat 2013) with Maikki Karisto and a cookbook with Saara Nironen and Nanna Tuovinen. Book is published first in Finnish, Sahramia, munia ja mantelimaitoa (Salakirjat 2012) and English version Saffron, Eggs and Almond Milk (Salakirjat 2014).

Contact: At events or mervi.pasanen@gmail.com

Online presence: Hibernaatiopesäke http://hibernaatio.blogspot.com

Apprentices/Students/Household affiliations: Apprentice: Lady Katarina Juhanantytär

Elevated by whom and when: Lief and Morrigan, July AS 49 (2014)

Apprenticed to: Helena von Eltz

Classes I have held/are willing to hold: I have held dress courses and I am happy to teach dyeing, tablet weaving and naalbinding. I do mainly 14th century, but Finnish iron age is my great passion and I'm happy to help with that too.

## Magistra Judith de Northumbria

Region: (Frankmark) Currently out-of-Kingdom.

Primary artforms/Elevated for: 15th and 16th c. dance/1470s-80s Florentine and Burgundian clothing

Dabbles in: Textile arts: Fingerlooping, embroidery, dyeing, cooking (using reproduction medieval equipment)

Contact: At events or judithsca@aol.com

Online presence: https://www.facebook.com/ladye.judith

Apprentices/Students/Household affiliations: Magdalena Grace Vane, Conandil Ingen Donngaile, Beibhin, William of Born, Katherine of South Downs

Elevated by whom and when: Elevated by Michael and Moira in 2006

Apprenticed to: Master Gregory Blount

Classes I have held/are willing to hold: 15th c./16th c/ social dance; 15th c.-16th c. Masque

## Maisteri Kaarina Eerikintytär

Region: Aarnimetsä, Canton of Poukka

Primary artforms/Elevated for: Music, singing and writing

Dabbles in: Dance

Contact: At events or taija.paju(at)gmail.com or facebook

Online presence: http://pajut.net/taijan/Cms.php?page=Medieval(ish)%20songs

Elevated by whom and when: Vitus III and Isabel II at Cudgel War, July 11, AS 53 (2018)

Classes I have held/are willing to hold: Music workshops (especially singing), analyzing a style as a tool for songwriting, reading period music manuscripts and musical notation, reading modern sheet music.

## Meisterinne Katheryn Hebenstreitz

Region: Aarnimetsä, Canton of Hukka

Primary artforms/Elevated for: Tailoring, German 16th century clothing

Dabbles in: Viking clothing, 14th century clothing, embroidery, dyeing, tablet weaving, naale binding, illumination and calligraphy

Contact: At events or at amadejska@gmail.com

Online presence: www.textiletimetravels.org

Apprentices/Students/Household affiliations: Apprentices: Mistress  Johanna af Hukka, Duchess Siobhán inghean uí Liatháin.

Protégés: Lady Åsa Fredriksdotter, Lady Ragnell Caxtone, Baron UlfR Knutsson

Elevated by whom and when: 2 of June 2011 by Vitus and Eleonora

Classes I have held/are willing to hold: German 16th century female clothing (I prefer to teach one on one in this subject so that I can give the information specific to the persons needs), intarsia embroidery, posament, The stuchlein – how to make it, Black work for German 16th century clothing, German renaissance clothing in context, The Lübeck Frauenwams.

## Mestari Kareina Talventytär

Region: Nordmark

Primary artforms/Elevated for: Hand-sewing and embroidery

Dabbles in: Have since become addicted to nålbinding. Am happiest with doing clothing styles ranging from really, really early (like stone age sewing techniques are also of interest) to the 12th Century, prefer northern Europe. Also do: dance, cooking, baking.

Contact: At events or kareina.sca@gmail.com

Online Presence: https://kareina.dreamwidth.org

Apprentice/s: Ena Iliansdotter, Astrid uti Erismarc

Elevated by whom and when: 2000,  Fabian and Bryn, King and Queen of the West

Classes I have held/are willing to hold: Hand sewing, laid and couched work embroidery, early period costume construction logic, dance, wax tablet making, cooking from period recipes

## Dame Lyonet de Covenham
Region: Insulae Draconis, near Guildford, England 

Primary artforms/Elevated for: Heralding, scrivening, scholarship, poesy

Dabbles in: Working in wood and leather, storytelling, the science of the sword

Contact: @nettie on the Drachenwald Slack, on Facebook or sca@nusbacher.com

Online Presence: Occasional contributor to the Dragon’s Scriptorium, to the Dragon Scribes FB Group, active on the Drachenwald and Insulae Draconis groups on Facebook, as well as the FB group, Drachenwald Heralds:  Behind the Curtain. 

Apprentices/Students/Household affiliations:  A dame of House de Taahe, a great household centred in Ealdormere and the Middle Kingdom. Dependents: Countess Isabel Peregrinus, Lady Oriana nic Kendrick, Lady Angelica Archer, Lady Ursula of Guildford.

Elevated by whom and when: Siridean Šah and Jahanara Bambišn, Yule Ball at Buckden in Flintheath, December an. soc. lij (2017)

Apprenticed to:  Once squire and student to Duke Finnvarr de Taahe

Classes I have held/are willing to hold: Basic Latin calligraphy workshop, Latin handwriting style workshops (for particular hands including textura quadrata, textura precissa, littera bastarda, uncial and other forms), Basic Hebrew calligraphy workshop (including Sephardic and Ashkenazic styles), Making quill and reed pens, Mediaeval chancery processes, Fealty and Homage in mediaeval Europe, Heralds and heralding in the Middle Ages and Renaissance, Sir King and his Court in the 14th Century, The organisation of mediaeval royal courts, Heralding in the Society, Heralding in the Society (advanced:  courts), Heralding in the Society (advanced:  tournaments), The triad of Mediaeval warfare, Tactics of early modern battle, Change and consistency in mediaeval warfare, (Also, happy to teach various mediaeval and Renaissance fencing masters)

## Mistress Lia de Thornegge
Region: Nordmark, shire of Aros

Primary artforms/Elevated for: Late period embroidery and costuming. 

Dabbles in: 15th Century costuming, illumination and calligraphy, Counted thread embroidery, textile stuff in general

Contact: Email lia.thornegge@gmail.com or talk to me at events

Online presence: https://liadethornegge.dreamwidth.org/ http://thornegge.wordpress.com/ https://www.facebook.com/LiaDeThornegge/

Apprentices/Students/Household affiliations: Member of the households Brighthelm and Three Scissors and Duck. Apprentices: Lady Silwa af Swaneholm

Elevated by whom and when: Paul and Aryanhwy at Kingdom University November 2012

Apprenticed to: Helwig Ulfsdotter

Classes I have held/are willing to hold: Blackwork embroidery, 16th Century shirts, Doublet fitting workshop with Mistress Helwig and apprentices, Elizabethan Metal Thread embroidery, How to learn a calligraphy style, Long-armed Cross Stitch

## Mistress Mary verch Thomas

Region: Insulae Draconis, Shire of Mynydd Gwyn

Primary artforms/Elevated for: Embroidery

Dabbles in: garb-making, finger-loop braiding

Contact: at events or mary at maryfrost (dot) me (dot) uk

Online presence: http://www.maryfrost.me.uk/

Apprentices/Students/Household affiliations: Household - Brighthelm

Elevated by whom and when: Siridean and Jahanara at Raglan Faire, 12 Aug AS 52

Classes I have held/are willing to hold: Goldwork, detached buttonhole, general embroidery, simple fingerloop braiding. Feel free to ask me.

## Mistress Melisende Fitzwalter

Region: Insulae Draconis

Primary artforms/Elevated for: Calligraphy and Illumination

Dabbles in: Can also advise on period cookery. Other areas of interest include embroidery and music.

Contact: At events or melisende1380@yahoo.co.uk

Apprentices/Students/Household affiliations: Apprentice: Viscount Eirik Hårfager.  Student: Lady Arianhwy Wen.  Head of House Fitzwalter, member of House Fevre and the Households of Brighthelm and House O'Mona.

Elevated by whom and when: 2010-05-29 by Thorvaldr and Fiona

Apprenticed to: Etienne Fevre

Classes I have held/are willing to hold: Various aspects of Calligraphy & Illumination, Cookery and Feast kitchen organisation. 

## Mistress Oriane d'Avallon

Region: Insulae Draconis

Primary artforms/Elevated for: Calligraphy and Illumination

Dabbles in: Enamelling and lace making.

Contact: At events or oriane@web.de

Apprentices/Students/Household affiliations: Member of House Gilchrest

Elevated by whom and when: 22.05.1998 by Matthew and Anna at Double Wars VI

Classes I have held/are willing to hold: Calligraphy & Illumination, Gilding.

## Mistress Petronilla of London

Region: Barony of Knights' Crossing, Canton of Turmstadt

Primary artforms/Elevated for: Embroidery

Dabbles in: sewing, tailoring, cooking and general research

Contact: At events or facebook or petronilla.of.london(at)gmail.com

Online Presence: http://elishevaskitchen.blogspot.com, www.flickr.com/photos/ladypetronilla/

Apprentices/Students/Household affiliations: Member of House Hebenstreitz

Elevated by whom and when: Jahanara and Siridean, Kingdom University November 11, AS 52 (2017)

Apprenticed to: Katheryn Hebenstreitz

Classes I have held/are willing to hold: General embroidery, Opus Anglicanum embroidery, Or Nué embroidery, other specific types of embroidery if needed, Medieval healthcare theory in cooking

## Meisterinne Renike Tucher

Region: Nordmark, Barony Styringheim

Primary artforms/Elevated for: Textile Arts / 15th Century costuming

Dabbles in: A lot of things, like embroidery, crossbow and archery, illumination and research

Contact: At events or menglad@swipnet.se

Online Presence: Renika's Anachronistic Adventure

Apprentices/Students/Household affiliations: Apprentice - Lady Lali Ingemarsdotter

Elevated by whom and when: Morales and Agnes at Baronial Investiture & Fiesta de Lucia in Styringheim, December 12 A.S. 51 (2016)

Apprenticed to: Joutsenjärven Sahra

Classes I have held/are willing to hold:  fitting, patternmaking, freeform embroidery, late 15th century German costuming

## Mistress Rogned Steingrimovna

Region: Eplaheimr, Insulae Draconis

Primary artforms/Elevated for: Textile construct and embellishment

Dabbles in: Everything textile, cloth and clothing construct, embellishment, physical science, medicine, and music.

Contact: At events or nisiocan@gmail.com

Apprentices/Students/Household affiliations: Jahanara Vivana (apprentice), Christine Bess Duvant (apprentice), Kytte of the Lake (apprentice).

Elevated by whom and when: Their Royal Majesties Davin and Groa of AnTir AS XXXIV, January 8 at Twelfth Night, 2000

Apprenticed to: none

Classes I have held/are willing to hold:  Njalbinding, weaving, embroidery, spinning, dying, sprang, clothing construction, Slavic beading and embroidery. I am happy to help judge competitions of most genre when I can be at an event.

## Mistress Signe Scriffuerska

Region: Nordmark, Shire of Aros

Primary artforms/Elevated for: Calligraphy & Illumination

Dabbles in: Dance, singing, making costumes (mainly 15th century)

Contact: At events

Apprentices/Students/Household affiliations:

Elevated by whom and when: Lief and Morrigan, Nordmark Midvinter and 30 Year Celebration, 20th of December 2014

Classes I have held/are willing to hold: Anything about illumination, I have given these classes among other things: Heraldry for the scribe, How to make your own gesso, How to make period scroll, Scrolls for beginners, 15th century costume.

## Master Skafte Vaghorne

Region: Nordmark, Barony of Styringheim

Primary artforms/Elevated for: Performing instrumental Music

Dabbles in: Calligraphy

Contact: At events or skafte@styringheim.se

Apprentices/Students/Household affiliations: Apprentices -  Fru Erid Ingefridsdotter Hinspaka and Herr Fledar Skote, getfarmare från Lojsta

Elevated by whom and when: 1999 by Matthew and Anna

Apprenticed to: Nobody, but protegé to Mistress Hawas Arna Krämplösa

Classes I have held/are willing to hold: Show and tell about woodwind instruments from the Viking age to the Renaissance.


## Master Sven Gunnarsson of Fiathrundrialand

Region: Nordmark, currently out of Kingdom 

Primary artforms/Elevated for: Armour making

Dabbles in: Everything

Contact: At events

Apprentices/Students/Household affiliations: Apprentice: Lord Ludewic Nilsson

Elevated by whom and when: 1999 by Prothall and Cecilia

## Master SvartulvR Kåte 

Region: Nordmark

Primary artforms/Elevated for: Casting, smithing, woodcarving etc.

Contact: At events or anton_ronnblom@hotmail.com

Online presence: http://www.ronnblom.nu

Apprentices/Students/Household affiliations: Earl Peregrine McKay, Johan Knutsson, Lord John Smith, Baroness Estrid Salten-Sebestid

Elevated by whom and when: 2011 by Lief and Morrigan

Apprenticed to: Haakon Pikinokka

Classes I have held/are willing to hold: Period casting, period and modern smithing, period shoe making, box making, class on how to make period smithing/casting/cooking ovens. Armour making etc.

## Mistress Tece de Kaxtone

Region: Nordmark, Shire of Holmrike

Primary artforms/Elevated for: Textile arts/Tailoring.

Dabbles in: 15th and 16th Century costuming, mainly german, Viking costume main focus Gotland, Counted thread embroidery, Tablet weaving, Research of whatever topic that looks interesting, Textile related things in general and finding and hoarding feast gear.

Contact: At events or tecedekaxtone at gmail.com

Online Presence: http://tecedekaxtone.blogspot.se/

facebook.com/therese.pettersson.503

Apprentices/Students/Household affiliations: N/A

Elevated by whom and when: Agnes and Morales at Kingdom University in Insulae Draconis A.S. LI, being 12th of November 2016.

Apprenticed to: Lia de Thornegge

Classes I have held/are willing to hold:Smocking, Brick stitch and Blackwork embroidery, Fitting hose, Fabric knowledge aka let it burn! If it is something I do and seem to know well enough I can probably be persuaded to hold a class or lecture about it.

## Master Thomas Flamanc of Kelsale

Region: Insulae Draconis

Primary artforms/Elevated for: Period Cookery and its Methods

Dabbles in: Shoe making and I can advise on 11th c English and Norse clothing as well as 15th C English styles. Also woodturning and woodworking.

Contact: At events or  tfkelsale@gmail.com

Online presence: http://jpgsawyer.weebly.com/home.html

Apprentices/Students/Household affiliations: Honorary Member of House Woodrose (Lochac). Prentices Lady Maude d'Elsynge, Lady Emoni de la Fere.

Elevated by whom and when: 2012 by Sven and Siobhan

Classes I have held/are willing to hold: Camp Cookery, general period cookery, beginners shoemaking, making a bread oven or any of the topics above.

## Mistress Tófa Jóhansdóttir

Region: Aarnimetsä

Primary artforms/Elevated for: Viking period Finnish costuming

Dabbles in: Viking period weaving & sewing. I am also interested in medieval food and drinks in Finland and medieval gardening.

Contact: At events

Elevated by whom and when: 2003-12 by Marcus and Anna

## Mistress Uta Boucht

Region: Aarnimetsä

Primary artforms/Elevated for: Craft and Mystery of Tailoring

Dabbles in: Close to my heart are textiles and clothing in general, including embroidery, and mid 14th c. in particular with a special love for everything French.

Contact: At events or uusitalo(at)yahoo.com

Online presence: http://utankamari.net/

Apprentices/Students/Household affiliations:

Elevated by whom and when: 2012-02 by Sven and Siobhan

Apprenticed to: Mistress Alienor of Farryngdon

Classes I have held/are willing to hold: Various classes on 14th century clothing and accessories (See more at http://utankamari.net under "Teaching")

## Master Wiglaf Wilfriding (sometimes spelled uuiglaf or uuiglaf ridere)

Region: Ad Flumen, Frankmark (Austria)

Primary artforms/Elevated for: A little bit of everything, but mostly leatherwork and dance.  Also armoring, metalwork, and illumination

Dabbles in: See above

Contact: At events

Household: No apprentices (two squires, though).

Elevated by whom and when: West Kingdom 12th Night, 1993 (Rolf and Mari)

Apprenticed to: none (I was knighted long before I was laurelled; squired to Sten Halvorsen).

Classes I have held/are willing to hold: I can't sew.  I have tried really hard, but I can't.  So don't ask.